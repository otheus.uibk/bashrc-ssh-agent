###
### Setup the ssh-agent (intelligently) if needed
###

## version 1.0
## author: otheus@gmail.com

## Use cases:
##  1. SSH forwarded from remote host.
##     * update agent.env with SSH_AUTH_SOCK
##     * setup read_ssh_agent
##     * (fall-through)
##  2. non-tty session
##     * read_ssh_agent
##     * (no more processing)
##  3. login session (SHLVL=1)
##     * setup read_ssh_agent if it doesnt exist
##     * read_ssh_agent
##     * trap SIGWINCH
##     3a. if SSH_AUTH_SOCK is invalid
##         and if PID exists but is invalid
##         * start ssh-agent and update agent.env
##     * (no more processing)
##  4. subshell (or screen session)
##     * assumes parent process set things up properly
##       (reattached screen session terminals will not work here)
##     * setup read_ssh_agent if it doesnt exist
##     * read_ssh_agent
##     * trap SIGWINCH

#### begin configuration

### common to all: setup agent.env variable (dont clobber existing value)
[[ $ssh_agent_env ]] || export ssh_agent_env="$HOME/.ssh/agent.env"
is_login_shell() {
    # For linux GUIs, you will typically change 1 to 2.
    [[ $SHLVL = 1 ]];
}
ps_check_agent() {
    # linux-specific and possibly distro-specific.
    ps -q "${1:-1}" -o comm= | grep -qx ssh-agent ;
}

#### end configuration

### common to all use-cases: setup ssh_load_agent_info (iff needed)
if ! type -t ssh_load_agent_info ; then
  ssh_load_agent_info(){ test -f "$ssh_agent_env" && source "$ssh_agent_env" ;}
  export -f ssh_load_agent_info
fi

### 1. remote shell with forwarded agent
if is_login_shell && [[ $SSH_AUTH_SOCK ]] && [[ -S $SSH_AUTH_SOCK ]] ; then
  echo "SSH_AUTH_SOCK=$SSH_AUTH_SOCK; export SSH_AUTH_SOCK" > "$ssh_agent_env"
fi

if ! tty &>/dev/null ; then
  ### 2. non-interactive login
  ssh_load_agent_info
else
  if is_login_shell; then
    ### 3. login session
    # the read-ssh-agent fucntion will return false if the file
    # is not found or not readable.
    if  ! ssh_load_agent_info &&
        ! [[ -S "$SSH_AUTH_SOCK" ]] &&
        ! ps_check_agent "${SSH_AGENT_PID}"
    then
      ### 3a. if agent.env is invalid, restart agent and regenerate agent.env
      ssh-agent | grep -v ^echo > "$ssh_agent_env"
      ssh_load_agent_info
    fi
  else
    ### 4. subshell/screen session: only read agent file. ignore errors.
    ssh_load_agent_info
  fi

  ### use cases 1, 3, 4
  trap ssh_load_agent_info SIGWINCH
fi
unset ps_check_agent is_login_shell

# optional:
# export PROMPT_COMMAND="ssh_load_agent_info"
# ssh-add -l &>/dev/null || ssh-add
