# A bashrc scriptlet to help you with ssh-agent

## Introduction

One of the features of openssh is a helper utility called `ssh-agent`, which 
securely remembers your private key so that you musn't type it in every 
time you connect to a remote system. In the gitlab/github era, this feature
has become rather crucial. For evey git pull and push, you must otherwise
enter your password. 

One of the key problems with this utility is that you must tell each terminal
how to connect to it. That is, after you will launch the 
ssh-agent from a terminal, the next terminal (on the same workstation or server)
which you log into won't know how to communicate with that ssh-agent until you 
update some environment variables. So either
you must start a new ssh-agent and re-add your key, or you go back to the other
terminal (which terminal was it?) to get that meta-data (what was the name of
those variables?) and copy and paste them into the new terminal (which mouse 
button pastes?). Indeed, this bashrc scriptlet is itself an ironic expression of 
laziness.

And then, if you are like me, you use one terminal session to remotely connect
to the _real_ system where you do your work and from which you must use git+ssh
to facilitate your task. For this latter case, openssh has a feature called
_authentication agent forwarding_. This way, you can run `ssh-agent` on your 
local workstation, and the server you connect to will have the credentials
available to it via a socket connection back to your workstation. But to get
that to work, you _must not_ overwrite the variables you would set up when 
running `ssh-agent` on the remote server. 

Finally, if you are a power-user like me, you use `screen` to virtualize your
terminals, and will often find yourself detaching and reattaching them from a
different workstation or laptop, needing to update the authentication variables 
all over again.

## Four use-cases

I've identified four basic usage scenarios. (For you youngins, a "use case" is 
a user-story for grown-ups.) These are from the standpoint of the bash process 
being run under the user's credentials on the _target_ host, ie, the one you 
will somehow log onto and then make outbound ssh connections _from_. However, 
you might have connected _to_ this host via ssh from a remote client.

1. Login occurred via a remote connection, which has authentication-forwarding enabled. 
The `$SSH_AUTH_SOCK` variable is set by the SSH daemon and passed to the shell.
This usage scenario is _not exclusive_ to the other three.

2. Login occured via a remote connection, which requested a non-interactive (ie, no TTY) session. 
This scnario is exclusive to those that follow. The user may want to use this 
connection for outbound SSH connections, but witout a TTY, no password entry
is possible (safely), and thus, only existing (or forwarded) ssh-agents may be used.

3. Ordinary interactive login session (which could be from a remote host or on the local console),
or the equivalent of an xterm on your Linux desktop. There may or may not be an
existing ssh-agent running and available. There might be remnants of one after a reboot
and we must clean up the old files.

4. A subshell of any of the above. The subshell should inherit the parent 
process' information, but the information may need to be refreshed.


## Solution

Basically, my solution (which is exclusively for bash users. zsh users, 
feel free to submit a working solution and I will link to it here) handles
the above scenarios with aplomb. It figures out if your SSH session is already
being forwarded, or if not, tries to connect to an existing `ssh-agent`, 
but if that isn't possible, starts a new one. (Either way, you must run 
`ssh-add` by hand). If you reattach to a `screen` session,
simply resize your GUI window and all your terminals will get the new environment
variable. (More on that below). 

Add this scriptlet to your $HOME and "source" it from your `.bashrc`. That is, append this to your
`.bashrc`:

    source $HOME/.bashrc-ssh-agent
    
Or whatever. Make sure your `.bash_profile.d` also does this, 
or (better yet) sources your `.bashrc`.

On your next login, check the output of `$SSH_AUTH_SOCK`. If you are using a
workstation, you should now have an `ssh-agent` process running. Either way,
you can now run

    ssh-add -l

to see a list of keys that are stored in the agent. If there are none 
(and if you don't expect there to be one), add it with: 

    ssh-add

## Manually updating the variables

At any time, if you need to, you can reload the SSH variables by executing:

    ssh_load_agent_info
    
If you need to start a new `ssh-agent`, you can kill the current one and log into
a new terminal. Or, you can do this by hand:

    ssh-agent | grep -v ^echo > "$ssh_agent_env"

## Screen details

I trap `SIGWINCH` to allow the shell to detect a change to the size of the window.
When you resize the GUI window, screen catches that signal and passes it onto the 
underlying shell (note: if a terminal is in an editor when you resize, the 
shell won't get the signal). For a more reliable approach, 
you can set `PROMPT_COMMAND` to the `read_ssh_agent` function.

    export PROMPT_COMMAND="ssh_load_agent_info"

While this solution is handy, there are some problems with it. See "Known Problems" below.
Additionally, people use PROMPT_COMMAND for weird things, and if they do it, either
they will unexpectedly clobber your setting above, or you will clobber theirs. 

## Environmental friendliness

These two labels will pollute your bash's environment:  `ssh_load_agent_info` and `ssh_agent_env`. 
Also, `SIGWINCH` is trapped.

## Known Problems

OK, so there's one tiny flaw here, especially if you use PROMPT_COMMAND. But also
if you use screen + window-resizing. If you connect _twice_ to the target host with ssh and with agent-forwarding enabled, 
the first one will get updated with the second's socket information. When you 
exit the second window, the target host's openssh process will clean up that socket
connection and remove the socket, which the first one still points to. The first one will then point
to a non-existent socket. You can manually fix this, but I too lazy to find a general solution.
